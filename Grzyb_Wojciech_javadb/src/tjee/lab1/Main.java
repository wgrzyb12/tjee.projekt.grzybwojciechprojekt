package tjee.lab1;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {
        String[] command = {"CMD", "/C", "java -jar .\\javadb\\lib\\derbyrun.jar ij"};
        ProcessBuilder pb = new ProcessBuilder(command);
        Process process = pb.start();
        OutputStream stdin = process.getOutputStream();
        InputStream stderr = process.getErrorStream();
        InputStream stdout = process.getInputStream();

        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));

        //Polaczenie z baza danych
        writer.write("CONNECT 'jdbc:derby:..\\Grzyb_Wojciech_db;create=true';"+"\n");
        writer.flush();

        //Utworzenie tabeli GRZYB_WOJCIECH_OKNA_TB oraz zaladowanie jej danymi
        writer.write("run '.\\scripts\\Grzyb_Wojciech_configure_db.sql';"+"\n");
        writer.flush();

        writer.write("exit;"+"\n");
        writer.flush();
        writer.close();

        try {
            int exitValue = process.waitFor();
            System.out.println(exitValue);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String line;
        BufferedReader reader = new BufferedReader (new InputStreamReader (stdout));
        while ((line = reader.readLine ()) != null) {
            System.out.println ("[Stdout] " + line);
        }
        reader.close();

        reader = new BufferedReader (new InputStreamReader (stderr));
        while ((line = reader.readLine ()) != null) {
            System.out.println ("[Stderr] " + line);
        }
        reader.close();
    }
}
