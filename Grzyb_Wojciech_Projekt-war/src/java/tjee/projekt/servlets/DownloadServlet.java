package tjee.projekt.servlets;

import java.io.BufferedOutputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceRef;
import tjee.projekt.war.fileTransferer.FileTransferer_Service;

@WebServlet(name = "DownloadServlet", urlPatterns = {"/DownloadServlet"})
public class DownloadServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/FileTransferer/FileTransferer.wsdl")
    private FileTransferer_Service service;
    
    private byte[] download(java.lang.String arg0) {
        tjee.projekt.war.fileTransferer.FileTransferer port = service.getFileTransfererPort();
        return port.download(arg0);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileName = request.getParameter("fileName");
        if(fileName == null || fileName.equals("")){
            throw new ServletException("File Name can't be null or empty");
        }
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        byte[] imageBytes = download(fileName);
	response.setContentType("image/jpg");
        try {
            ServletOutputStream fos = response.getOutputStream();
            BufferedOutputStream outputStream = new BufferedOutputStream(fos);
            outputStream.write(imageBytes);
            outputStream.close();
            fos.close();
        } catch (IOException ex) {
            System.err.println(ex);
            throw new WebServiceException(ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    }
    
    @Override
    public String getServletInfo() {
        return "Download";
    }    
}