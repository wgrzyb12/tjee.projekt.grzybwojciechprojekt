package tjee.projekt.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;
import tjee.projekt.war.fileTransferer.FileTransferer_Service;
import tjee.projekt.war.fileTransferer.Image;

@WebServlet(name = "ShowImageServlet", urlPatterns = {"/ShowImageServlet"})
public class ShowImageServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/FileTransferer/FileTransferer.wsdl")
    private FileTransferer_Service service;
    
    private Image getImg(java.lang.String fileName) {
        tjee.projekt.war.fileTransferer.FileTransferer port = service.getFileTransfererPort();
        return port.getImg(fileName);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String fileName) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Grzyb Wojciech - Projekt (Wyswietlanie ikony)</title>");
            out.println("<meta charset=\"UTF-8\">");
            out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.println("<link rel=\"icon\" type=\"image/png\" href=\"./Style/img/favicon.png\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/components.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/icons.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/responsee.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Wyswietlanie ikony</h1>");
            out.println("<h3>"+"Nazwa: "+fileName+"</h3>");
            Image image = getImg(fileName);
            if(image!=null) {
                out.println("<img src=\""+image.getImgPath()+"\">");
            } else {
                out.println("Wystapil blad. Ikona nie istnieje.</br>");
            }
            out.println("<a class='button rounded-btn cancel-btn s-2 margin-bottom left' href=\"ShowImagesServlet\"><i class='icon-sli-arrow-left' style=\"font-size:15px\"></i>Powrót</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileName = request.getParameter("fileName");
        if(fileName == null || fileName.equals("")){
            throw new ServletException("File Name can't be null or empty");
        }
        processRequest(request, response,fileName);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

    @Override
    public String getServletInfo() {
        return "Show Image";
    }
}
