package tjee.projekt.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;
import tjee.projekt.war.fileTransferer.FileTransferer_Service;

@WebServlet(name = "RemoveImageServlet", urlPatterns = {"/RemoveImageServlet"})
public class RemoveImageServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/FileTransferer/FileTransferer.wsdl")
    private FileTransferer_Service service;

    private void removeImg(java.lang.String fileName) {
        tjee.projekt.war.fileTransferer.FileTransferer port = service.getFileTransfererPort();
        port.removeImg(fileName);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileName = request.getParameter("fileName");
        if(fileName == null || fileName.equals("")){
            throw new ServletException("File Name can't be null or empty");
        }
        removeImg(fileName);
        response.sendRedirect("ShowImagesServlet");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

    @Override
    public String getServletInfo() {
        return "Remove Image";
    }
}
