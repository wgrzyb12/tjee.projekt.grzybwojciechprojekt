package tjee.projekt.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.ws.WebServiceRef;
import tjee.projekt.ejb.controller.LastImageUploadLocal;
import tjee.projekt.war.fileTransferer.FileTransferer_Service;

@WebServlet(name = "UploadServlet", urlPatterns = {"/UploadServlet"})
@MultipartConfig
public class UploadServlet extends HttpServlet {

	LastImageUploadLocal lastImageUpload = lookupLastImageUploadLocal();
    
    private LastImageUploadLocal lookupLastImageUploadLocal() {
        try {
            Context c = new InitialContext();
            return (LastImageUploadLocal) c.lookup("java:global/Grzyb_Wojciech_Projekt/Grzyb_Wojciech_Projekt-ejb/LastImageUpload!tjee.projekt.ejb.controller.LastImageUploadLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/FileTransferer/FileTransferer.wsdl")
    private FileTransferer_Service service;
    
    private boolean upload(java.lang.String arg0, byte[] arg1) {
        tjee.projekt.war.fileTransferer.FileTransferer port = service.getFileTransfererPort();
        return port.upload(arg0, arg1);
    }
    
    private String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1);
            }
        }
        return null;
    }
    
    private byte[] getFileContent(InputStream inputStream) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int reads = inputStream.read();
            while (reads != -1) {
                baos.write(reads);
                reads = inputStream.read();
            }
            return baos.toByteArray();
        } catch (UnsupportedEncodingException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException ex) {
                    System.err.println(ex);
                }
            }
        }
        return null;
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String message) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Grzyb Wojciech - Projekt (Formularz udostepniania ikon - status)</title>");
            out.println("<meta charset=\"UTF-8\">");
            out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.println("<link rel=\"icon\" type=\"image/png\" href=\"./Style/img/favicon.png\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/components.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/icons.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/responsee.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Formularz udostepniania ikon - status</h1>");
            out.println("<h3>"+message+"</h3>");
            out.println(lastImageUpload.getLastImageUpload());
            lastImageUpload.setLastImageUpload(request.getHeader( "user-agent"), new java.util.Date());
            out.println("<a class='button rounded-btn cancel-btn s-2 margin-bottom left' href=" + request.getContextPath() + "><i class='icon-sli-arrow-left' style=\"font-size:15px\"></i>Powrót</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part filePart = request.getPart("file");
        String fileName = getFilename(filePart);
        String mimeType=getServletContext().getMimeType(fileName);
        
        if (fileName != null && fileName.length() > 0 && mimeType != null && mimeType.startsWith("image/")) {
            InputStream inputStream = filePart.getInputStream();
            byte[] fileContent = getFileContent(inputStream);
            boolean status = upload(fileName,fileContent);
            processRequest(request, response, status ? "Ikona zostala udostepniona." : "Wystapil blad.");
        } else {
            processRequest(request, response, "Wystapil blad.");
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Upload";
    }
}