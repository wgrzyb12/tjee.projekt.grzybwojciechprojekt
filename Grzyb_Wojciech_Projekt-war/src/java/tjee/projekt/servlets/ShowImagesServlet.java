package tjee.projekt.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;
import tjee.projekt.war.fileTransferer.FileTransferer_Service;
import tjee.projekt.war.fileTransferer.Image;

@WebServlet(name = "ShowImagesServlet", urlPatterns = {"/ShowImagesServlet"})
public class ShowImagesServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/FileTransferer/FileTransferer.wsdl")
    private FileTransferer_Service service;
    
    private java.util.List<tjee.projekt.war.fileTransferer.Image> getAllImages() {
        tjee.projekt.war.fileTransferer.FileTransferer port = service.getFileTransfererPort();
        return port.getAllImages();
    }
        
    private String showImages(List<Image> images){
        if(images.isEmpty()) {
            return "<h3>Brak danych.</h3></br>";
        }
        String str="<table id=\"tab\">"
                + "<tr>"
                + "<th>ID</th>"
                + "<th>NAZWA</th>"
                + "<th>ROZMIAR</th>"
                + "<th>SZEROKOSC</th>"
                + "<th>WYSOKOSC</th>"
                + "<th>DATA</th>"
                + "<th>IKONA</th>"
                + "<th>WYSWIETL</th>"
                + "<th>POBIERZ</th>"
                + "<th>USUN</th>"
                + "</tr>";
        for(int i=0; i<images.size(); i++) {
            str += "<tr>";
            str += "<td>"+(i+1)+"</td>";
            str += "<td>"+images.get(i).getImgName()+"</td>";
            str += "<td>"+images.get(i).getImgSize()+"</td>";
            str += "<td>"+images.get(i).getImgWidth()+"</td>";
            str += "<td>"+images.get(i).getImgHeight()+"</td>";
            str += "<td>"+images.get(i).getImgDate()+"</td>";
            str += "<td>"+"<img src=\""+images.get(i).getImgPath()+"\" style=\"width:55px;height:55px\">"+"</td>";
            str += "<td>"+"<a class='button rounded-btn reload-btn' onclick=\"showImg(this)\"><i class='icon-sli-arrow-right' style=\"font-size:15px\"></i>Wyswietl</a>"+"</td>";
            str += "<td>"+"<a class='button rounded-btn submit-btn' onclick=\"downloadImg(this)\"><i class='icon-download' style=\"font-size:15px\"></i>Pobierz</a>"+"</td>";
            str += "<td>"+"<a class='button rounded-btn cancel-btn' onclick=\"removeImg(this)\"><i class='icon-cross_mark' style=\"font-size:15px\"></i>Usun</a>"+"</td>";
            str += "</tr>";
        }
        str += "</table>";
        return str;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Grzyb Wojciech - Projekt (Wyswietlanie wszystkich obrazkow)</title>");
            out.println("<meta charset=\"UTF-8\">");
            out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.println("<link rel=\"icon\" type=\"image/png\" href=\"./Style/img/favicon.png\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/components.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/icons.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/responsee.css\">");
            out.println("<script type='text/javascript'>\n");
            out.println("function downloadImg(element) {\n" +
                        "   var i=element.parentNode.parentNode.rowIndex;\n"+
                        "   var j=element.parentNode.cellIndex;\n"+
                        "   var imgName=document.getElementById(\"tab\").rows[i].cells[1].innerHTML;\n"+
                        "   location.href = \"DownloadServlet?fileName=\"+imgName;\n"+
                        "}\n");
            out.println("function removeImg(element) {\n" +
                        "   var i=element.parentNode.parentNode.rowIndex;\n"+
                        "   var imgName=document.getElementById(\"tab\").rows[i].cells[1].innerHTML;\n"+
                        "   location.href = \"RemoveImageServlet?fileName=\"+imgName;\n"+
                        "}\n");
            out.println("function showImg(element) {\n" +
                        "   var i=element.parentNode.parentNode.rowIndex;\n"+
                        "   var imgName=document.getElementById(\"tab\").rows[i].cells[1].innerHTML;\n"+
                        "   location.href = \"ShowImageServlet?fileName=\"+imgName;\n"+
                        "}\n");
            out.println("</script>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Wyswietlanie wszystkich obrazkow</h1>");
            out.println(showImages(getAllImages()));
            out.println("<a class='button rounded-btn cancel-btn s-2 margin-bottom left' href=" + request.getContextPath() + "><i class='icon-sli-arrow-left' style=\"font-size:15px\"></i>Powrót</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Show all images";
    }
}
