package tjee.projekt.ejb.controller;

import javax.ejb.Stateful;
import java.util.Date;

@Stateful
public class LastImageUpload implements LastImageUploadLocal {
    private String lastUploadData;
    private Date lastUploadDate;

    @Override
    public void setLastImageUpload(String data, Date date) {
        this.lastUploadData=data;
        this.lastUploadDate=date;
    }

    @Override
    public String getLastImageUpload() {
        String str="<table>"
                + "<tr>"
                + "<th>OSTATNIA DATA</th>"
                + "<th>OSTATNIE DANE</th>"
                + "</tr>"
                + "<tr>"
                + "<td>"+lastUploadDate+"</td>"
                + "<td>"+lastUploadData+"</td>"
                + "</tr>"
                + "</table>";
        return str;
    }
}
