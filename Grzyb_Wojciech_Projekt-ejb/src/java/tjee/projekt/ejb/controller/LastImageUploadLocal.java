package tjee.projekt.ejb.controller;

import java.util.Date;
import javax.ejb.Local;

@Local
public interface LastImageUploadLocal {

    void setLastImageUpload(String data, Date date);
    String getLastImageUpload();
    
}
