package tjee.projekt.jpa.image;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ImageFacade extends AbstractFacade<Image> implements ImageFacadeLocal {

    @PersistenceContext(unitName = "Grzyb_Wojciech_Projekt-jpaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ImageFacade() {
        super(Image.class);
    }
    
    @Override
    public List<Image> findByImgName(String imgName) {
        return getEntityManager().createQuery("SELECT i FROM Image i where i.imgName = :imgName").setParameter("imgName", imgName).getResultList();
    }
    
    @Override
    public Image find(String imgName) {
        List<Image> images = findByImgName(imgName);
        return images.isEmpty() ? null : (Image) images.get(0) ;
    }
}
