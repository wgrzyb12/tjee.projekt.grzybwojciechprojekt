package tjee.projekt.jpa.image;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "IMAGE")
@XmlRootElement
@NamedQueries({
      @NamedQuery(name = "Image.findAll", query = "SELECT i FROM Image i")
    , @NamedQuery(name = "Image.findByImgId", query = "SELECT i FROM Image i WHERE i.imgId = :imgId")
    , @NamedQuery(name = "Image.findByImgName", query = "SELECT i FROM Image i WHERE i.imgName = :imgName")
    , @NamedQuery(name = "Image.findByImgSize", query = "SELECT i FROM Image i WHERE i.imgSize = :imgSize")
    , @NamedQuery(name = "Image.findByImgWidth", query = "SELECT i FROM Image i WHERE i.imgWidth = :imgWidth")
    , @NamedQuery(name = "Image.findByImgHeight", query = "SELECT i FROM Image i WHERE i.imgHeight = :imgHeight")
    , @NamedQuery(name = "Image.findByImgDate", query = "SELECT i FROM Image i WHERE i.imgDate = :imgDate")
    , @NamedQuery(name = "Image.findByImgPath", query = "SELECT i FROM Image i WHERE i.imgPath = :imgPath")})
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IMG_ID")
    private Integer imgId;
    @Size(max = 30)
    @Column(name = "IMG_NAME")
    private String imgName;
    @Column(name = "IMG_SIZE")
    private Integer imgSize;
    @Column(name = "IMG_WIDTH")
    private Integer imgWidth;
    @Column(name = "IMG_HEIGHT")
    private Integer imgHeight;
    @Column(name = "IMG_DATE")
    @Temporal(TemporalType.DATE)
    private Date imgDate;
    @Size(max = 50)
    @Column(name = "IMG_PATH")
    private String imgPath;

    public Image(String imgName, Integer imgSize, Integer imgWidth, Integer imgHeight, Date imgDate, String imgPath) {
        this.imgName = imgName;
        this.imgSize = imgSize;
        this.imgWidth = imgWidth;
        this.imgHeight = imgHeight;
        this.imgDate = imgDate;
        this.imgPath = imgPath;
    }

    public Image() {
    }

    public Image(Integer imgId) {
        this.imgId = imgId;
    }

    public Integer getImgId() {
        return imgId;
    }

    public void setImgId(Integer imgId) {
        this.imgId = imgId;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public Integer getImgSize() {
        return imgSize;
    }

    public void setImgSize(Integer imgSize) {
        this.imgSize = imgSize;
    }

    public Integer getImgWidth() {
        return imgWidth;
    }

    public void setImgWidth(Integer imgWidth) {
        this.imgWidth = imgWidth;
    }

    public Integer getImgHeight() {
        return imgHeight;
    }

    public void setImgHeight(Integer imgHeight) {
        this.imgHeight = imgHeight;
    }

    public Date getImgDate() {
        return imgDate;
    }

    public void setImgDate(Date imgDate) {
        this.imgDate = imgDate;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imgId != null ? imgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Image)) {
            return false;
        }
        Image other = (Image) object;
        if ((this.imgId == null && other.imgId != null) || (this.imgId != null && !this.imgId.equals(other.imgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tjee.projekt.jpa.image.Image[ imgId=" + imgId + " ]";
    }
}
