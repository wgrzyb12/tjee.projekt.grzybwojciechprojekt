package tjee.projekt.jpa.image;

import java.util.List;
import javax.ejb.Local;

@Local
public interface ImageFacadeLocal {
    void create(Image image);
    void edit(Image image);
    void remove(Image image);
    void remove(List<Image> image);
    Image find(Object id);
    Image find(String imgName);
    List<Image> findByImgName(String imgName);
    List<Image> findAll();
    List<Image> findRange(int[] range);
    int count();
}
