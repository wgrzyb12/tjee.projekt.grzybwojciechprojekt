package tjee.projekt.ws.fileTransferer;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.MTOM;
import tjee.projekt.jpa.image.Image;
import tjee.projekt.jpa.image.ImageFacadeLocal;

@MTOM
@WebService(serviceName="FileTransferer")
@Stateless
public class FileTransferer {
    @EJB
    private ImageFacadeLocal imageFacade;
    
    @WebMethod(operationName="upload")
    public boolean upload(@WebParam(name = "fileName") String fileName, @WebParam(name = "imageBytes") byte[] imageBytes) {
        String filePath = Constants.SERVER_SAVE_PATH + File.separator + fileName;
        try {
            FileOutputStream fos = new FileOutputStream(filePath);
            BufferedOutputStream outputStream = new BufferedOutputStream(fos);
            outputStream.write(imageBytes);
            outputStream.close();
            imageFacade.remove(imageFacade.findByImgName(fileName));
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
            Image image = new Image(fileName,imageBytes.length,img.getWidth(),img.getHeight(),new java.util.Date(),"workspace\\"+fileName);
            imageFacade.create(image);
        } catch (IOException ex) {
            System.err.println(ex);
            return false;
        }
        return true;
    }
     
    @WebMethod(operationName="download")
    public byte[] download(@WebParam(name = "fileName") String fileName) {
        String filePath = Constants.SERVER_SAVE_PATH + File.separator + fileName;
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream inputStream = new BufferedInputStream(fis);
            byte[] fileBytes = new byte[(int) file.length()];
            inputStream.read(fileBytes);
            inputStream.close();
            return fileBytes;
        } catch (IOException ex) {
            System.err.println(ex);
            throw new WebServiceException(ex);
        }
    }
	
    @WebMethod(operationName="getAllImages")
    public List<Image> getAllImages(){
        return imageFacade.findAll();
    }
	
    @WebMethod(operationName="getImg")
    public Image getImg(@WebParam(name = "fileName") String fileName){
        return imageFacade.find(fileName);
    }
    
    @WebMethod(operationName="removeImg")
    public void removeImg(@WebParam(name = "fileName") String fileName){
        String filePath = Constants.SERVER_SAVE_PATH + File.separator + fileName;
        File file = new File(filePath);
        file.delete();
        imageFacade.remove(imageFacade.find(fileName));
    }
}
